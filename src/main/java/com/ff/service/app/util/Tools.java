package com.ff.service.app.util;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import static java.lang.System.arraycopy;


public class Tools {

    public enum MSG_TYPE {
        NOTICE, WARNING, EXCEPTION;

        public String toString() {
            switch (this.ordinal()) {
                case 0:
                    return "NOTICE";
                case 1:
                    return "WARNING";
                case 2:
                    return "EXCEPTION";
                default:
                    return null;
            }
        }

        ;
    }

    //	public enum CONNECTION_STATUS{CS_CONNECTED, CS_CONNECTING, CS_DISCONNECTED};
    public enum SIDE {RIGHT, LEFT}

    ;

    public Tools() {
        //TODO SecretKeySpec, Cipher 事先create 比免每次create 浪費resource , 但需考慮密碼不得變更
        //TODO Deflater, Inflater 事先create 比免每次create 浪費resource
        //TODO ByteArrayOutputStream

    }

    private final static byte _NUL = 0x20;

    /**
     * AES(256) 加密(jdk不支援，要更新local_policy.jar,US_export_policy.jar才能使用)
     * AES(128) 加密
     *
     * @param key
     * @param src
     * @return
     * @throws Exception
     */
    public static byte[] AES_Encrypt(String key, byte[] src, String mode) throws Exception {
        byte[] encrypt = null;
        byte[] temp;
        try {
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
            Cipher cipher;
            if (mode.equals("NoPadding")) {
                cipher = Cipher.getInstance("AES/ECB/NoPadding");

                if ((src.length % 16) != 0)
                    temp = new byte[(src.length / 16 + 1) * 16];
                else
                    temp = new byte[src.length];
                Arrays.fill(temp, _NUL);
                System.arraycopy(src, 0, temp, 0, src.length);
            } else {
                cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
                temp = src;
            }
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
            encrypt = cipher.doFinal(temp);
        } catch (Exception e) {
            System.out.println(e);
        }
        /*
         * Base64(-1)：長度超過預設76時不換行，因為C++那邊不接受
         * 編碼後的數據比原始數據略長，為原來的4/3
         */
        temp = Base64.getEncoder().encode(encrypt);    //2015-09-18
        return temp;  //return 密文
    }

    /**
     * AES(128) 解密
     * AES(256) 解密(jdk不支援，要更新local_policy.jar,US_export_policy.jar才能使用)
     *
     * @param key
     * @param src
     * @return
     * @throws Exception
     */
    public static byte[] AES_Decrypt(String key, byte[] src, String mode) throws Exception {
        byte[] decrypt = null;
        try {
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

            Cipher cipher;
            if (mode.equals("NoPadding")) {
                cipher = Cipher.getInstance("AES/ECB/NoPadding");
            } else {
                cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            }
            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
//			byte[] temp = new Base64().decode(src);
            byte[] temp = Base64.getDecoder().decode(src);    //2015-09-18
            decrypt = cipher.doFinal(temp);
        } catch (Exception e) {
            System.out.println(e);
        }
        return decrypt;
    }

    public static String MD5(String src) {
        //MD5編碼,由右邊開始填滿b值,由左邊開始填滿b值,
        String password = src;
        BigInteger bi = null;
        //System.out.println("MD5encode="+password);
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());

            byte digest[] = md.digest();

            bi = new BigInteger(1, digest);
        } catch (Exception e) {
            System.out.println("編碼失敗");
        }
        return strFixLen(bi.toString(16), "0", 32, SIDE.LEFT);
//	      return bi.toString(16);
    }

    /**
     * 記憶體轉十六進位字串
     *
     * @param {byte[]} buf 資料
     * @return String 十六進位字串
     * @author albert
     */
    public static String asHex(byte buf[]) {
        StringBuffer strbuf = new StringBuffer(buf.length * 2);
        for (int i = 0; i < buf.length; i++) {
            if (((int) buf[i] & 0xff) < 0x10)
                strbuf.append("0");

            strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
        }

        return strbuf.toString();
    }


    /**
     * ZLib壓縮
     *
     * @param {byte[]} uncomporess 未壓縮資料
     * @return {byte[]} 壓縮後資料
     * @author albert
     */
    public static byte[] compress(byte[] uncomporess) {
        Deflater compressor = new Deflater();
        compressor.setLevel(Deflater.BEST_COMPRESSION);
        compressor.setInput(uncomporess);
        compressor.finish();
        ByteArrayOutputStream bos = new ByteArrayOutputStream(uncomporess.length);

        byte[] buf = new byte[1024];
        while (!compressor.finished()) {
            int count = compressor.deflate(buf);
            bos.write(buf, 0, count);
        }
        try {
            bos.close();
        } catch (IOException e) {
        }

        return bos.toByteArray();
    }

    /**
     * ZLib解壓縮資料
     *
     * @param {byte[]} compress 壓縮資料
     * @return {byte[]} Uncompress 解壓縮後資料
     * @throws UnsupportedEncodingException
     * @throws IOException
     * @throws DataFormatException
     * @author albert
     */
    public static byte[] uncompress(byte[] compress) throws UnsupportedEncodingException, IOException, DataFormatException {
        Inflater ifl = new Inflater();
        ifl.setInput(compress);
        ByteArrayOutputStream baos = new ByteArrayOutputStream(compress.length);
        byte[] buff = new byte[1024];
        while (!ifl.finished()) {
            int count = ifl.inflate(buff);
            baos.write(buff, 0, count);
        }
        baos.close();

        return baos.toByteArray();
    }

    /**
     * 補足字串長度
     *
     * @param oStr   原始字串
     * @param aStr   新增字元
     * @param len    補足後總長度
     * @param {SIDE} side 靠左或靠右補齊(RIGHT:補右邊, LEFT:還是補左邊)
     * @return 回覆字串
     * @author albert
     * @since
     */
    public static final String strFixLen(String oStr, String aStr, int len, SIDE side) {
        if (oStr.length() >= len)
            return oStr;

        String newStr = "";
        byte[] bOrg = oStr.getBytes();
        byte[] bAdd = aStr.getBytes();
        byte[] bNew = new byte[len - bOrg.length];

        for (int i = 0; i < bNew.length; i++) {
            if ((i + 1) % bAdd.length == 0)
                bNew[i] = bAdd[bAdd.length - 1];
            else
                bNew[i] = bAdd[((i + 1) % bAdd.length) - 1];
        }

        if (side == SIDE.LEFT)
            newStr = new String(bNew) + oStr;
        else
            newStr = oStr + new String(bNew);

        return newStr;
    }

    /**
     * 轉換 unsigned byte (-126..127) 轉為 int(0..255)
     *
     * @param {byte} num signed-byte
     * @return {int} unsigned-byte
     */
    public static final int uByte(byte num) {
        return (int) num & 0xFF;
    }


    /**
     * 整數 Short 轉為Byte Array 並 HI-LOW byte 轉換
     *
     * @param num 整數
     * @return 2 bytes array
     */
    public static final byte[] int2Bytes(short num) {
        return Int2Bytes((long) num, 2);
    }

    /**
     * 整數 Int 轉為Byte Array 並 HI-LOW byte 轉換
     *
     * @param num 整數
     * @return 4 bytes array
     */
    public static final byte[] int2Bytes(int num) {
        return Int2Bytes((long) num, 4);
    }

    /**
     * 整數 long 轉為Byte Array 並 HI-LOW byte 轉換
     *
     * @param num 整數
     * @return 8 bytes array
     */
    public static final byte[] int2Bytes(long num) {
        return Int2Bytes(num, 8);
    }

    /**
     * 傳入 byte[] 回傳 long
     *
     * @param num
     * @return
     */
    public final long getLong(byte[] num) {
        return ((((long) Tools.uByte(num[0])) << 56)
                | (((long) Tools.uByte(num[1])) << 48)
                | (((long) Tools.uByte(num[2])) << 40)
                | (((long) Tools.uByte(num[3])) << 32)
                | (((long) Tools.uByte(num[4])) << 24)
                | (((long) Tools.uByte(num[5])) << 16)
                | (((long) Tools.uByte(num[6])) << 8)
                | (((long) Tools.uByte(num[7])) << 0));
    }


    /**
     * 數字轉為Byte Array, 並Hi-Low Byte 轉換
     *
     * @param num 整數
     * @return Byte Array
     */
    private static final byte[] Int2Bytes(long num, int size) {
        byte[] rtn = null;
        switch (size) {
            case 2:
                rtn = new byte[2];
                rtn[1] = (byte) (num >> (8 * 1));
                rtn[0] = (byte) (num);
                break;
            case 4:
                rtn = new byte[4];
                rtn[3] = (byte) (num >> (8 * 3));
                rtn[2] = (byte) (num >> (8 * 2));
                rtn[1] = (byte) (num >> (8 * 1));
                rtn[0] = (byte) (num);
                break;
            case 8:
                rtn = new byte[8];
                rtn[7] = (byte) (num >> (8 * 7));
                rtn[6] = (byte) (num >> (8 * 6));
                rtn[5] = (byte) (num >> (8 * 5));
                rtn[4] = (byte) (num >> (8 * 4));
                rtn[3] = (byte) (num >> (8 * 3));
                rtn[2] = (byte) (num >> (8 * 2));
                rtn[1] = (byte) (num >> (8 * 1));
                rtn[0] = (byte) (num);
                break;
        }

        return rtn;
    }

    /**
     * High-Low byte 轉換 for Integer
     *
     * @param {int} num 原始數字
     * @return {int} 轉換後數字
     */
    public static final int HiLoByteSwap4Short(int num) {
        return (num >> 8) + (num & 0x000000FF) * 256;
    }

    /**
     * Hi-Lo byte 轉換 for 2 byte[]
     *
     * @param {byte[]} num 原始數字記憶體
     * @return {int} 轉換後數字
     */
    public static final int HiLoByteSwap4TwoBytes(byte[] num) {
        return (int) (uByte(num[0]) + uByte(num[1]) * 256);
    }

    /**
     * Byte Array Hi-Low byte轉換後, 轉為整數
     *
     * @param src 來源ByteArray
     * @param idx 起始位置
     * @param len 長度
     * @return 整數
     */
    public static final long Bytes2Int(byte[] src, int idx, int len) {
        if ((len > 8) || (len < 1))
            return 0;

        long rtn = uByte(src[idx]);
        for (int i = 1; i < len - 1; i++)
            rtn = (long) (rtn + (uByte(src[i + idx]) * Math.pow(256, i)));

        return rtn;
    }

    /**
     * Byte Array Hi-Low byte轉換後, 轉為整數
     *
     * @param src 來源ByteArray
     * @return 整數
     */
    public static final long Bytes2Int(byte[] src) {
        return Bytes2Int(src, 0, src.length);

    }

    /**
     * byte[] 指定位置長度轉換為整數
     *
     * @param {byte[]} src 原記憶體資料
     * @param {int}    idx 起始位置
     * @param {int}    len 長度 1:byte  2:short  4:integer  8:long
     * @return 整數
     * @author albert
     * @since
     */
    public static final long HiLoByteSwap4Bytes(byte[] src, int idx, int len) {
        long rtn = 0;
        byte[] tmp = new byte[len];
        arraycopy(src, idx, tmp, 0, len);
        switch (len) {
            case 1:
                rtn = uByte(tmp[0]);
                break;
            case 2:
                rtn = (uByte(src[1]) + 256 + uByte(src[0]));
                break;
            case 4:
                rtn = (uByte(src[3]) * 256 * 256 * 256
                        + uByte(src[2]) * 256 * 256
                        + uByte(src[1]) * 256
                        + uByte(src[0]));
                break;
            case 8:
                rtn = ((long) uByte(src[7]) * 256 * 256 * 256 * 256 * 256 * 256 * 256
                        + (long) uByte(src[6]) * 256 * 256 * 256 * 256 * 256 * 256
                        + (long) uByte(src[5]) * 256 * 256 * 256 * 256 * 256
                        + (long) uByte(src[4]) * 256 * 256 * 256 * 256
                        + (long) uByte(src[3]) * 256 * 256 * 256
                        + (long) uByte(src[2]) * 256 * 256
                        + (long) uByte(src[1]) * 256
                        + (long) uByte(src[0]));
                break;
            default:
                break;
        }
        tmp = null;
        return rtn;
    }

    public static int unsignedByte(byte b) {
        return (int) b & 0xFF;
    }

    /**
     * 轉換為 int
     *
     * @param num
     * @return
     */
    public static int switchByte(byte[] num) {
        return (int) (unsignedByte(num[0]) + unsignedByte(num[1]) * 256);
    }

    /**
     * 轉換為 int
     *
     * @param num
     * @return
     */
    public static int switchInt(byte[] num) {
        return (int) (unsignedByte(num[0])
                + unsignedByte(num[1]) * 256
                + unsignedByte(num[2]) * 256 * 256
                + unsignedByte(num[3]) * 256 * 256 * 256);
    }

    /**
     * 轉換為long
     *
     * @param num
     * @return
     */
    public static long switchLong(byte[] num) {

        long requestId = Tools.uByte(num[0]);
        requestId = requestId + Tools.uByte(num[1]) * 256;
        requestId = requestId + Tools.uByte(num[2]) * (long) Math.pow(256, 2);//256的 2,3,4,5,6,7 次方
        requestId = requestId + Tools.uByte(num[3]) * (long) Math.pow(256, 3);
        requestId = requestId + Tools.uByte(num[4]) * (long) Math.pow(256, 4);
        requestId = requestId + Tools.uByte(num[5]) * (long) Math.pow(256, 5);
        requestId = requestId + Tools.uByte(num[6]) * (long) Math.pow(256, 6);
        requestId = requestId + Tools.uByte(num[7]) * (long) Math.pow(256, 7);
        return requestId;
    }

    public static int ByteA2IntSwitchByte(byte[] src, int index, int offset) {
        int rtn = 0;
        byte[] tmp = new byte[offset];
        System.arraycopy(src, index, tmp, 0, offset);
        switch (offset) {
            case 1:
                rtn = unsignedByte(tmp[0]);
                break; //byte
            case 2:
                rtn = switchByte(tmp);
                break;  //short
            case 4:
                rtn = switchInt(tmp);
                break;   //int

        }
        return rtn;
    }


    /**
     * 根據 offset (位置) 判斷轉換方法, 回傳 long
     *
     * @param src
     * @param index
     * @param offset
     * @return
     */
    public final long ByteA2LongSwitchByte(byte[] src, int index, int offset) {
        long rtn = 0;
        byte[] tmp = new byte[offset];
        System.arraycopy(src, index, tmp, 0, offset);
        switch (offset) {
            case 1:
                rtn = Tools.uByte(tmp[0]);
                break; //byte
            case 2:
                rtn = Tools.switchByte(tmp);
                break;  //short
            case 4:
                rtn = Tools.switchInt(tmp);
                break;    //int
            case 8:
                rtn = Tools.switchLong(tmp);
                break;   //long
        }
        return rtn;
    }

    /**
     * 捨去小數4位，回傳整數價格
     *
     * @param {String} doolar
     * @return {String}
     * @author kelly
     */
    public static String Trim0Dollar(String dollar) {
        for (int i = 0; i < dollar.length(); i++) {
            if (!dollar.substring(i, i + 1).equals("0")) {
                dollar = dollar.substring(i);
                break;
            }
        }
        if (!dollar.trim().equals("")) {
            dollar = dollar.substring(0, dollar.length() - 4);
        } else {
            dollar = "0";
        }
        return dollar;
    }

    /**
     * PACKED BCD 轉 Long
     */
    public static long BCDtoLong(byte[] src, int idx, int len) {
        long data = 0;
        int i = 0;
        for (; i < len; i++, ++idx) {
            byte b = src[idx];

            byte high = (byte) (b & 0xf0);
            high >>>= (byte) 4;
            high = (byte) (high & 0x0f);
            byte low = (byte) (b & 0x0f);

            data = data * 100 + (high * 10 + low);
        }
        return data;
    }

    /*
     * long number to bcd byte array e.g. 123 --> (0000) 0001 0010 0011
     * e.g. 12 ---> 0001 0010
     */
    public static byte[] DecToBCDArray(long num) {
        int digits = 0;

        long temp = num;
        while (temp != 0) {
            digits++;
            temp /= 10;
        }

        int byteLen = digits % 2 == 0 ? digits / 2 : (digits + 1) / 2;
        boolean isOdd = digits % 2 != 0;

        byte bcd[] = new byte[byteLen];

        for (int i = 0; i < digits; i++) {
            byte tmp = (byte) (num % 10);

            if (i == digits - 1 && isOdd)
                bcd[i / 2] = tmp;
            else if (i % 2 == 0)
                bcd[i / 2] = tmp;
            else {
                byte foo = (byte) (tmp << 4);
                bcd[i / 2] |= foo;
            }

            num /= 10;
        }

        for (int i = 0; i < byteLen / 2; i++) {
            byte tmp = bcd[i];
            bcd[i] = bcd[byteLen - i - 1];
            bcd[byteLen - i - 1] = tmp;
        }

        return bcd;
    }

    public static String BCDtoString(byte bcd) {
        StringBuffer sb = new StringBuffer();

        byte high = (byte) (bcd & 0xf0);
        high >>>= (byte) 4;
        high = (byte) (high & 0x0f);
        byte low = (byte) (bcd & 0x0f);

        sb.append(high);
        sb.append(low);

        return sb.toString();
    }

    public static String BCDtoString(byte[] bcd) {
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < bcd.length; i++) {
            sb.append(BCDtoString(bcd[i]));
        }

        return sb.toString();
    }


    /**
     * 傳入 int 回傳 byte[]
     *
     * @param num
     * @param size
     * @return
     */
    private static byte[] switchIntToByteA(int num, int size) {
        byte[] b = new byte[size];
        switch (size) {
            case 4:
                b[3] = (byte) (0xFF & (num >>> 24));
            case 3:
                b[2] = (byte) (0xFF & (num >>> 16));
            case 2:
                b[1] = (byte) (0xFF & (num >>> 8));
            case 1:
                b[0] = (byte) (0xFF & num);
        }
        return b;
    }

    /**
     * 傳入 long 回傳 byte[]
     *
     * @param num
     * @param size
     * @return
     */
    private byte[] switchLongToByteA(long num, int size) {
        byte[] b = new byte[size];
        switch (size) {
            case 8:
                b[7] = (byte) (0xFF & (num >>> 56));
                b[6] = (byte) (0xFF & (num >>> 48));
                b[5] = (byte) (0xFF & (num >>> 40));
                b[4] = (byte) (0xFF & (num >>> 32));
            case 4:
                b[3] = (byte) (0xFF & (num >>> 24));
            case 3:
                b[2] = (byte) (0xFF & (num >>> 16));
            case 2:
                b[1] = (byte) (0xFF & (num >>> 8));
            case 1:
                b[0] = (byte) (0xFF & num);
        }
        return b;
    }

    public byte[] switchIntToByteA(long num) {
        return switchLongToByteA(num, 8);
    }

    public byte[] switchIntToByteA(int num) {
        return switchIntToByteA(num, 4);
    }

    public static byte[] switchIntToByteA(short num) {
        return switchIntToByteA(num, 2);
    }

    public byte[] switchIntToByteA(byte num) {
        return switchIntToByteA(num, 1);
    }

    /**
     * 透過 request bytes 取得 IP
     * @param b
     * @param begin
     * @return
     */
    protected String parseIp(byte[] b,int begin) {
        int i1 = b[begin] & 0xFF;
        int i2 = b[begin+1] & 0xFF;
        int i3 = b[begin+2] & 0xFF;
        int i4 = b[begin+3] & 0xFF;
        StringBuffer sb = new StringBuffer();
        sb.append(i1).append(".");
        sb.append(i2).append(".");
        sb.append(i3).append(".");
        sb.append(i4);
        return sb.toString();
    }
}

