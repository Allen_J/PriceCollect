package com.ff.service.app.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "p529")
public class P529Entity {

    @EmbeddedId
    private P529PK p529PK;

    @Column(name = "type", insertable = false, updatable = false)
    private int type;

    @Lob
    @Column(name = "content", insertable = false, updatable = false)
    private String content;

    @Column(name="created_datetime")
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    private LocalDateTime created_datetime;

    @Column(name="updated_datetime")
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    private LocalDateTime updated_datetime;

    @Embeddable
    @NoArgsConstructor
    @AllArgsConstructor
    public static class P529PK implements Serializable {

        private int type;

        private String content;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            P529PK p529PK = (P529PK) o;
            return type == p529PK.type && content.equals(p529PK.content);
        }

        @Override
        public int hashCode() {
            return Objects.hash(type, content);
        }
    }
}
