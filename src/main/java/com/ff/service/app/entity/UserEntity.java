package com.ff.service.app.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * 放一個有複合主鍵的table參考
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user")
public class UserEntity {

    @EmbeddedId
    private UserPK userPK;

    @Column(name = "email")
    private String email;

    @Column(name = "created_datetime")
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    private LocalDateTime createdDateTime;

    @Column(name = "updated_datetime")
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    private LocalDateTime updatedDateTime;

    @Embeddable
    @AllArgsConstructor
    @NoArgsConstructor
    public static class UserPK implements Serializable {

        private String cellPhone;
        private String username;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            UserPK userPK = (UserPK) o;
            return cellPhone.equals(userPK.cellPhone) && username.equals(userPK.username);
        }

        @Override
        public int hashCode() {
            return Objects.hash(cellPhone, username);
        }
    }

}
