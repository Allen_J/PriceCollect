package com.ff.service.app.protocol.packet;

import com.ff.service.app.enums.MessageEnums;
import com.ff.service.app.protocol.packet.base.BasePacket;
import com.ff.service.app.util.Tools;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * created by user21 on 2021/7/6
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class P530 extends BasePacket {

    public static int LENGTH = 32;
    /**
     * 頭部訊息
     */
    private final int type = MessageEnums.FOMS_SMART_ORDER_RESPONSE.getType();
    /**
     * 0:成功
     */
    public short code = 0;
    /**
     * 使用者自訂，回覆時帶回
     */
    public byte[] userDef = new byte[12];
    /**
     * 分公司代號
     */
    public byte[] brokerID = new byte[3];
    /**
     * 帳號
     */
    public byte[] account = new byte[7];
    /**
     * 委託錯誤碼 X(5)
     */
    public byte[] errCode = new byte[5];
    /**
     * 訊息長度
     */
    public byte[] msgLen = new byte[3];
    /**
     * 錯誤訊息
     */
    public byte[] errMsg = null;

    public P530(byte[] body) {
        int idx = 0;
        int len = 0;
        code = (short) Tools.ByteA2IntSwitchByte(body, 0, len = 2);
        idx += len;
        System.arraycopy(body, idx, userDef, 0, len = userDef.length);
        idx += len;
        System.arraycopy(body, idx, brokerID, 0, len = brokerID.length);
        idx += len;
        System.arraycopy(body, idx, account, 0, len = account.length);
        idx += len;
        System.arraycopy(body, idx, errCode, 0, len = errCode.length);
        idx += len;
        System.arraycopy(body, idx, msgLen, 0, len = msgLen.length);
        idx += len;
        int errMsgLength = Integer.valueOf(new String(msgLen));
        if (errMsgLength > 0) {
            errMsg = new byte[errMsgLength];
            System.arraycopy(body, idx, errMsg, 0, len = errMsgLength);
            idx += len;
        }
    }

    @Override
    public byte[] toBytes() {
        int idx = 0;
        int len = 0;
        byte[] body = new byte[length()];
        System.arraycopy(Tools.switchIntToByteA(code), 0, body, idx, len = 2);
        idx += len;
        System.arraycopy(userDef, 0, body, idx, len = userDef.length);
        idx += len;
        System.arraycopy(brokerID, 0, body, idx, len = brokerID.length);
        idx += len;
        System.arraycopy(account, 0, body, idx, len = account.length);
        idx += len;
        System.arraycopy(errCode, 0, body, idx, len = errCode.length);
        idx += len;
        System.arraycopy(msgLen, 0, body, idx, len = msgLen.length);
        idx += len;
        if (errMsg != null) {
            System.arraycopy(errMsg, 0, body, idx, len = errMsg.length);
            idx += len;
        }
        return body;
    }

    @Override
    public int length() {
        return (errMsg == null) ? LENGTH : LENGTH + errMsg.length;
    }
}
