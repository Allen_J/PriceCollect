package com.ff.service.app.protocol.packet.base;

import java.nio.charset.Charset;

/**
 * created by user21 on 2021/7/6
 * 電文物件基類
 *
 * 規範每個電文需要有
 * toBytes[] 方法
 * length 最大長度
 */
public abstract class BasePacket {

    /**
     * UTF-8編碼集，盡量少用String.getBytes("UTF-8")
     */
    protected static final Charset CHAR_SET_UTF8 = Charset.forName("UTF-8");
    /**
     * Big5編碼集，盡量少用String.getBytes("Big5")
     */
    protected static final Charset CHAR_SET_BIG5 = Charset.forName("Big5");

    /**
     * 抽象方法 toBytes[] ，輸出物件 byte[]
     * @return
     */
    public abstract byte[] toBytes();

    /**
     * 抽象方法 length,給出物件長度
     * @return
     */
    public abstract int length();


}
