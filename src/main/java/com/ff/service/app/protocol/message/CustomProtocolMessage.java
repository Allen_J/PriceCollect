package com.ff.service.app.protocol.message;

import com.ff.service.app.protocol.packet.base.BasePacket;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * created by user21 on 2021/7/6
 * 訊息封裝類,增加字尾
 * P529,P530,P531,P532
 */
@Data
@NoArgsConstructor
public class CustomProtocolMessage extends BasePacket {


    private int type;        // 檔頭
    private int length;      // 長度
    private byte[] content;  // 資料主體

    /**
     * 結束符號
     */
    //private final byte[] DATA_END = "\n".getBytes();

    /**
     * 在建構函數建立實體，不直接丟對象進來的原因是，沒有比直接處理 byte[] 方便
     * @param array
     * @param length
     * @param type
     */
    public CustomProtocolMessage(byte[] array, int length, int type) {
        byte[] data = new byte[length];
        int idx = 0;
        int len = 0;
        System.arraycopy(array, 0, data, idx, len = array.length);
        idx += len;
        this.content = data;
        this.length = idx;
        this.type = type;
    }

    /**
     * 以 byte[] 輸出現有物件
     *
     * @return
     */
    @Override
    public byte[] toBytes() {
        byte[] data = new byte[content.length];
        int idx = 0;
        int len = 0;
        System.arraycopy(content, 0, data, idx, len = content.length);
        idx += len;
        this.length = idx;
        return data;
    }

    @Override
    public int length() {
        return this.length;
    }
}
