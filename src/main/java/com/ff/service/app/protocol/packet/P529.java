package com.ff.service.app.protocol.packet;

import com.ff.service.app.enums.MessageEnums;
import com.ff.service.app.protocol.packet.base.BasePacket;
import com.ff.service.app.util.Tools;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * created by user21 on 2021/7/6
 */
@Data
@AllArgsConstructor
public class P529 extends BasePacket {


    /**
     * 長度, 不包含簽章
     */
    private static final int LENGTH = 233;
    /**
     * 頭部訊息
     */
    private final int type = MessageEnums.FOMS_SMART_ORDER_REQUEST.getType();
    /**
     * 'O':新單 'C':刪單
     */
    private byte[] funcCode = new byte[1];
    /**
     * 分公司代號 9(3) F001XXX
     */
    private byte[] brokerID = new byte[3];
    /**
     * 帳號9(7)
     */
    private byte[] account = new byte[7];
    /**
     * 部門 X(4)
     */
    private byte[] department = new byte[4];
    /**
     * 子帳號 / 交易員 X(7)
     */
    private byte[] subAcc = new byte[7];
    /**
     * 交易所 X(8)
     */
    private byte[] exchange = new byte[8];
    /**
     * 買賣別1 X(1) B:買, S:賣
     */
    private byte[] bs1 = new byte[1];
    /**
     * 買賣別2 X(1) B:買, S:賣
     */
    private byte[] bs2 = new byte[1];
    /**
     * 商品代號 X(7)
     */
    private byte[] commodity = new byte[7];
    /**
     * 商品月份 9(6) YYYYMM
     */
    private byte[] comYM = new byte[6];
    /**
     * 商品月份2 9(6) YYYYMM 第二隻腳的月份
     */
    private byte[] comYM2 = new byte[6];
    /**
     * 履約價 9(7)V9(6)
     */
    private byte[] strikePri = new byte[13];
    /**
     * 買賣權 X(1) C:買權, P:賣權, F:期貨
     */
    private byte[] cp = new byte[1];
    /**
     * 委託口數1 9(4)
     */
    private byte[] qty1 = new byte[4];
    /**
     * 觸發價格1 9(7)V9(6)
     */
    private byte[] strategyPrice1 = new byte[13];
    /**
     * 委託口數2 9(4)
     */
    private byte[] qty2 = new byte[4];
    /**
     * 觸發價格2 9(7)V9(6)
     */
    private byte[] strategyPrice2 = new byte[13];
    /**
     * 價格別1 X(1) 1:市價, 2:限價
     */
    private byte[] priType1 = new byte[1];
    /**
     * 價格別2 X(1) 1:市價, 2:限價
     */
    private byte[] priType2 = new byte[1];
    /**
     * 委託條件 X(1) F:FOK, I:IOC, R:ROD
     */
    private byte[] timeInForce = new byte[1];
    /**
     * 新平倉碼 X(1) O:新倉, C:平倉, ‘ ‘自動
     */
    private byte[] openClose = new byte[1];
    /**
     * 當沖 X(1) Y/N
     */
    private byte[] dayTrade = new byte[1];
    /**
     * 委託日期 9(8) YYYYMMDD
     */
    private byte[] orderDate = new byte[8];
    /**
     * 委託時間 9(9) HHMMSSsss
     */
    private byte[] orderTime = new byte[9];
    /**
     * 虛擬單號 X(10) (刪單時使用)
     */
    private byte[] seriesNo = new byte[10];
    /**
     * 觸發方向1 X(1) 0:小於 1:小於等於 2:等於 3:大於等於 4:大於
     */
    private byte[] triggerType1 = new byte[1];
    /**
     * 觸發方向2 X(1) 0:小於 1:小於等於 2:等於 3:大於等於 4:大於
     */
    private byte[] triggerType2 = new byte[1];
    /**
     * 觸發類別 X(1): A:追價, T:折返
     */
    private byte[] triggerKind = new byte[1];
    /**
     * 點數設定 X(4)
     */
    private byte[] triggerTicks = new byte[4];
    /**
     * 保留欄位 X(8)
     */
    private byte[] temp = new byte[8];
    /**
     * 使用者自訂 X(12)
     */
    private byte[] userDef = new byte[12];
    /**
     * 簽章資料長度
     */
    private short signatureLen = 0;
    /**
     * 簽章資料
     */
    private byte[] signature = new byte[signatureLen];

    /**
     * 按照讀入的 byte[] 逐各填入值
     *
     * @param body
     */
    public P529(byte[] body) {
        int idx = 0;
        int len = 0;
        System.arraycopy(body, idx, funcCode, 0, len = funcCode.length);
        idx += len;
        System.arraycopy(body, idx, brokerID, 0, len = brokerID.length);
        idx += len;
        System.arraycopy(body, idx, account, 0, len = account.length);
        idx += len;
        System.arraycopy(body, idx, department, 0, len = department.length);
        idx += len;
        System.arraycopy(body, idx, subAcc, 0, len = subAcc.length);
        idx += len;
        System.arraycopy(body, idx, exchange, 0, len = exchange.length);
        idx += len;
        System.arraycopy(body, idx, bs1, 0, len = bs1.length);
        idx += len;
        System.arraycopy(body, idx, bs2, 0, len = bs2.length);
        idx += len;
        System.arraycopy(body, idx, commodity, 0, len = commodity.length);
        idx += len;
        System.arraycopy(body, idx, comYM, 0, len = comYM.length);
        idx += len;
        System.arraycopy(body, idx, comYM2, 0, len = comYM2.length);
        idx += len;
        System.arraycopy(body, idx, strikePri, 0, len = strikePri.length);
        idx += len;
        System.arraycopy(body, idx, cp, 0, len = cp.length);
        idx += len;
        System.arraycopy(body, idx, qty1, 0, len = qty1.length);
        idx += len;
        System.arraycopy(body, idx, strategyPrice1, 0, len = strategyPrice1.length);
        idx += len;
        System.arraycopy(body, idx, qty2, 0, len = qty2.length);
        idx += len;
        System.arraycopy(body, idx, strategyPrice2, 0, len = strategyPrice2.length);
        idx += len;
        System.arraycopy(body, idx, priType1, 0, len = priType1.length);
        idx += len;
        System.arraycopy(body, idx, priType2, 0, len = priType2.length);
        idx += len;
        System.arraycopy(body, idx, timeInForce, 0, len = timeInForce.length);
        idx += len;
        System.arraycopy(body, idx, openClose, 0, len = openClose.length);
        idx += len;
        System.arraycopy(body, idx, dayTrade, 0, len = dayTrade.length);
        idx += len;
        System.arraycopy(body, idx, orderDate, 0, len = orderDate.length);
        idx += len;
        System.arraycopy(body, idx, orderTime, 0, len = orderTime.length);
        idx += len;
        System.arraycopy(body, idx, seriesNo, 0, len = seriesNo.length);
        idx += len;
        System.arraycopy(body, idx, triggerType1, 0, len = triggerType1.length);
        idx += len;
        System.arraycopy(body, idx, triggerType2, 0, len = triggerType2.length);
        idx += len;
        System.arraycopy(body, idx, triggerKind, 0, len = triggerKind.length);
        idx += len;
        System.arraycopy(body, idx, triggerTicks, 0, len = triggerTicks.length);
        idx += len;
        System.arraycopy(body, idx, temp, 0, len = temp.length);
        idx += len;
        System.arraycopy(body, idx, userDef, 0, len = userDef.length);
        idx += len;
        signatureLen = (short) Tools.ByteA2IntSwitchByte(body, idx, len = 2);
        idx += len;
        if (signatureLen > 0) {
            signature = new byte[signatureLen];
            System.arraycopy(body, idx, signature, 0, len = signature.length);
            idx += len;
        }
    }

    /**
     * 將自己的屬性寫入 byte[] body 並回傳
     *
     * @return
     */
    @Override
    public byte[] toBytes() {
        int idx = 0;
        int len = 0;
        byte[] body = new byte[length()];
        System.arraycopy(funcCode, 0, body, idx, len = funcCode.length);
        idx += len;
        System.arraycopy(brokerID, 0, body, idx, len = brokerID.length);
        idx += len;
        System.arraycopy(account, 0, body, idx, len = account.length);
        idx += len;
        System.arraycopy(department, 0, body, idx, len = department.length);
        idx += len;
        System.arraycopy(subAcc, 0, body, idx, len = subAcc.length);
        idx += len;
        System.arraycopy(exchange, 0, body, idx, len = exchange.length);
        idx += len;
        System.arraycopy(bs1, 0, body, idx, len = bs1.length);
        idx += len;
        System.arraycopy(bs2, 0, body, idx, len = bs2.length);
        idx += len;
        System.arraycopy(commodity, 0, body, idx, len = commodity.length);
        idx += len;
        System.arraycopy(comYM, 0, body, idx, len = comYM.length);
        idx += len;
        System.arraycopy(comYM2, 0, body, idx, len = comYM2.length);
        idx += len;
        System.arraycopy(strikePri, 0, body, idx, len = strikePri.length);
        idx += len;
        System.arraycopy(cp, 0, body, idx, len = cp.length);
        idx += len;
        System.arraycopy(qty1, 0, body, idx, len = qty1.length);
        idx += len;
        System.arraycopy(strategyPrice1, 0, body, idx, len = strategyPrice1.length);
        idx += len;
        System.arraycopy(qty2, 0, body, idx, len = qty2.length);
        idx += len;
        System.arraycopy(strategyPrice2, 0, body, idx, len = strategyPrice2.length);
        idx += len;
        System.arraycopy(priType1, 0, body, idx, len = priType1.length);
        idx += len;
        System.arraycopy(priType2, 0, body, idx, len = priType2.length);
        idx += len;
        System.arraycopy(timeInForce, 0, body, idx, len = timeInForce.length);
        idx += len;
        System.arraycopy(openClose, 0, body, idx, len = openClose.length);
        idx += len;
        System.arraycopy(dayTrade, 0, body, idx, len = dayTrade.length);
        idx += len;
        System.arraycopy(orderDate, 0, body, idx, len = orderDate.length);
        idx += len;
        System.arraycopy(orderTime, 0, body, idx, len = orderTime.length);
        idx += len;
        System.arraycopy(seriesNo, 0, body, idx, len = seriesNo.length);
        idx += len;
        System.arraycopy(triggerType1, 0, body, idx, len = triggerType1.length);
        idx += len;
        System.arraycopy(triggerType2, 0, body, idx, len = triggerType2.length);
        idx += len;
        System.arraycopy(triggerKind, 0, body, idx, len = triggerKind.length);
        idx += len;
        System.arraycopy(triggerTicks, 0, body, idx, len = triggerTicks.length);
        idx += len;
        System.arraycopy(temp, 0, body, idx, len = temp.length);
        idx += len;
        System.arraycopy(userDef, 0, body, idx, len = userDef.length);
        idx += len;
        System.arraycopy(Tools.switchIntToByteA(signatureLen), 0, body, idx, len = 2);
        idx += len;
        if (signature != null) {
            System.arraycopy(signature, 0, body, idx, len = signature.length);
            idx += len;
        }
        return body;
    }

    /**
     * 回傳靜態長度
     *
     * @return
     */
    @Override
    public int length() {
        return (signature == null) ? LENGTH : LENGTH + signature.length;
    }
}
