package com.ff.service.app.service;

import com.ff.service.app.handler.CustomProtocolServiceHandler;
import com.ff.service.app.handler.codec.CustomProtocolDecoder;
import com.ff.service.app.handler.codec.CustomProtocolEncoder;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;

/**
 * created by user21 on 2021/7/7
 */
@Slf4j
public class CustomProtocolServiceInitializer extends ChannelInitializer {


    private ApplicationContext applicationContext;

    public CustomProtocolServiceInitializer(ApplicationContext applicationContext){

        this.applicationContext = applicationContext;
    }

    @Override
    protected void initChannel(Channel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        //pipeline.addLast(new DelimiterBasedFrameDecoder(8192, Delimiters.lineDelimiter()));
        pipeline.addLast(new CustomProtocolDecoder());
        pipeline.addLast(new CustomProtocolEncoder());
        // pipeline.addLast(new StringDecoder(CharsetUtil.UTF_8));
        // pipeline.addLast(new StringEncoder(CharsetUtil.UTF_8));
        // pipeline.addLast(new CustomProtocolServiceHandler());
        pipeline.addLast(applicationContext.getBean(CustomProtocolServiceHandler.class)); //配合IOC必要作法，透過 applicationContext 取得 handler

    }

}

