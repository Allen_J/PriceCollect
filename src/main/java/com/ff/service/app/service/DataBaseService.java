package com.ff.service.app.service;

import com.ff.service.app.entity.P529Entity;
import com.ff.service.app.protocol.message.CustomProtocolMessage;
import com.ff.service.app.repository.P529Repository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * created by user21 on 2021/7/13
 */
@Slf4j
@Service
public class DataBaseService {

    @Autowired
    private P529Repository p529Repository;

    public void saveP529(CustomProtocolMessage msg) {

        try {
            P529Entity p529Entity = new P529Entity();
            P529Entity.P529PK p529PK = new P529Entity.P529PK(msg.getType(), new String(msg.getContent()));
            p529Entity.setP529PK(p529PK);
            p529Repository.save(p529Entity);
            log.info("save success: {}",p529Entity);
        } catch (Exception e) {
            log.error("{}", e);
        }

    }

}
