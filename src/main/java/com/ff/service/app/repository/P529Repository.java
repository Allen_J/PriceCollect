package com.ff.service.app.repository;

import com.ff.service.app.entity.P529Entity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface P529Repository extends JpaRepository<P529Entity, P529Entity.P529PK> {
}
