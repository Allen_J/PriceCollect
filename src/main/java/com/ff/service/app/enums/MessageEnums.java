package com.ff.service.app.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 存放消息檔頭與屬性
 */
@Getter
@AllArgsConstructor
public enum MessageEnums {

    FOMS_SMART_ORDER_REQUEST((byte) 91, 234),//長度含簽章
    FOMS_SMART_ORDER_RESPONSE((byte) 92, 32),
    UNKNOWN((byte) 255, 255);

    private byte type;
    private int length;

    public static MessageEnums valueOf(byte type) {
        for (MessageEnums instance : values()) {
            if (instance.type == type) {
                return instance;
            }
        }
        return UNKNOWN;
    }
}
