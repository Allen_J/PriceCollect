package com.ff.service.app.handler.codec;

import com.ff.service.app.protocol.message.CustomProtocolMessage;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import lombok.extern.slf4j.Slf4j;

/**
 * created by user21 on 2021/7/8
 */
@Slf4j
public class CustomProtocolEncoder extends MessageToByteEncoder<CustomProtocolMessage> {

    @Override
    protected void encode(ChannelHandlerContext ctx, CustomProtocolMessage msg, ByteBuf out) throws Exception {

        // FOMSSmartOrderMessage 是消息協定，拆為三部份: header, length, content

//        if (msg == null || msg.getMessageType() != MessageType.FOMS_SMART_ORDER_REQUEST.getCode()) {
//            throw new Exception("The encode message is null");
//        }

        byte[] content = msg.getContent();//取得資料
        int length = content.length;
        //寫入
        out.writeInt(msg.getType());//header
        out.writeInt(length);//length
        out.writeBytes(content);//content
        log.info("encoder: msgType:{}", msg.getType());
        log.info("encoder: length:{}", length);
        log.info("encoder: msg.toBytes():{}", msg.getContent());

    }
}

