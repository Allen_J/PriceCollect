package com.ff.service.app.handler.codec;


import com.ff.service.app.enums.MessageEnums;
import com.ff.service.app.protocol.message.CustomProtocolMessage;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * created by user21 on 2021/7/8
 */
@Slf4j
public class CustomProtocolDecoder extends ByteToMessageDecoder {

    private static final int BASE_LENGTH = MessageEnums.FOMS_SMART_ORDER_REQUEST.getLength();

    private int type;
    private int length;


    public void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        //log.info("in: {}", in);

        //可读长度必须大于基本长度
        if (in.readableBytes() >= BASE_LENGTH) {
            // 防止socket字节流攻击
            // 防止，客户端传来的数据过大
            // 因为，太大的数据，是不合理的
            if (in.readableBytes() > 2048) {
                in.skipBytes(in.readableBytes());
            }
            // 记录包头开始的index
            int beginReader;

            while (true) {
                // 获取包头开始的index
                beginReader = in.readerIndex();
                // 标记包头开始的index
                in.markReaderIndex();
                // 读到了协议的开始标志，结束while循环
                type = in.readInt();
                if (type == MessageEnums.FOMS_SMART_ORDER_REQUEST.getType() || type == MessageEnums.FOMS_SMART_ORDER_RESPONSE.getType()) {
                    log.info("find type:{}",type);
                    break;
                }

                // 未读到包头，略过一个字节
                // 每次略过，一个字节，去读取，包头信息的开始标记
                in.resetReaderIndex();
                in.readByte();

                // 当略过，一个字节之后，
                // 数据包的长度，又变得不满足
                // 此时，应该结束。等待后面的数据到达
                if (in.readableBytes() < BASE_LENGTH) {
                    return;
                }
            }

            // 消息的长度
            length = in.readInt();
            // 判断请求数据包数据是否到齐
            if (in.readableBytes() < length) {
                // 还原读指针
                in.readerIndex(beginReader);
                return;
            }

            // 读取data数据
            byte[] data = new byte[BASE_LENGTH];
            in.readBytes(data);

            CustomProtocolMessage msg = new CustomProtocolMessage(data, data.length,type);

            out.add(msg);
        }
    }

}

