package com.ff.service.app.handler;

import com.ff.service.app.protocol.message.CustomProtocolMessage;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;

import java.net.InetSocketAddress;

/**
 * created by user21 on 2021/7/6
 * 收529回530
 */
@Slf4j
public class CustomProtocolServiceHandler extends SimpleChannelInboundHandler<CustomProtocolMessage> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, CustomProtocolMessage msg) throws Exception {

        log.info("[server] 收到 obj: {}", msg);

        Channel incoming = ctx.channel();

        if (msg instanceof CustomProtocolMessage) {
            CustomProtocolMessage obj = (CustomProtocolMessage) msg;
            log.info("Client->Server: messageType {}", obj.getType());
            log.info("Client->Server: length {}", obj.getLength());
            log.info("Client->Server: content {}", obj.getContent());
            log.info("{}", obj);
            incoming.write(msg);

//            P530 p530 = new P530();
//            obj.setType(p530.getType());
//            obj.setLength(p530.length());
//            obj.setContent(p530.toBytes());
        }
//        log.info("Server->Client: 收到P529, 回覆P530: {}", msg);
        ctx.writeAndFlush(msg);
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        InetSocketAddress insocket = (InetSocketAddress) ctx.channel().remoteAddress();
        String clientIp = insocket.getAddress().getHostAddress();
        log.info("收到客戶端 [ip:" + clientIp + "] 連接");
    }

}
