CREATE SCHEMA IF NOT EXISTS demo;

CREATE TABLE IF NOT EXISTS demo.user(id long primary key, name varchar(255), key idx_name(name));