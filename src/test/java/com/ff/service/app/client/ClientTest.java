package com.ff.service.app.client;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * created by user21 on 2021/7/6
 */
public class ClientTest {


    private static final int POOL_SIZE_SEND = 1;

    /**
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {

        Executor executor = Executors.newFixedThreadPool(POOL_SIZE_SEND);
        for (int i = 0; i < POOL_SIZE_SEND; i++) {
            executor.execute(new ClientTask());
            Thread.sleep(1000);
        }

    }

}
