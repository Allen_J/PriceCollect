package com.ff.service.app.client;
import com.ff.service.app.handler.codec.CustomProtocolDecoder;
import com.ff.service.app.handler.codec.CustomProtocolEncoder;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;

/**
 *
 */
public class CustomProtocolClientInitializer extends ChannelInitializer {

    @Override
    protected void initChannel(Channel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        //pipeline.addLast(new DelimiterBasedFrameDecoder(8192, Delimiters.lineDelimiter()));
        pipeline.addLast(new CustomProtocolDecoder());
        pipeline.addLast(new CustomProtocolEncoder());
        // pipeline.addLast(new StringDecoder(CharsetUtil.UTF_8));
        // pipeline.addLast(new StringEncoder(CharsetUtil.UTF_8));
        pipeline.addLast(new CustomProtocolClientHandler());
    }
}
