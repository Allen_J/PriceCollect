package com.ff.service.app.client;

import com.ff.service.app.protocol.message.CustomProtocolMessage;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;

/**
 * created by user21 on 2021/7/6
 */
@Slf4j
public class CustomProtocolClientHandler extends SimpleChannelInboundHandler<CustomProtocolMessage> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, CustomProtocolMessage msg) throws Exception {

        log.info("obj: {}", msg);

        //  Channel incoming = ctx.channel();
        if (msg instanceof CustomProtocolMessage) {
            CustomProtocolMessage obj = (CustomProtocolMessage) msg;
            log.info("Server->Client: getType: {}, length: {}, content: {}", obj.getType(), obj.getLength(), obj.getContent());
        }
    }


    public void exceptionCaught(ChannelHandlerContext ctx) throws Exception {
        //ctx.flush();
    }

}