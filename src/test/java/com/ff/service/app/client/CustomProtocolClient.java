package com.ff.service.app.client;

import com.ff.service.app.protocol.packet.P529;
import com.ff.service.app.protocol.message.CustomProtocolMessage;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.StandardCharsets;

/**
 * created by user21 on 2021/7/6
 */
@Slf4j
public class CustomProtocolClient {

    private String host;
    private int port;

    private static final int MAX_FRAME_LENGTH = 1024 * 1024;
    private static final int LENGTH_FIELD_LENGTH = 4;
    private static final int LENGTH_FIELD_OFFSET = 6;
    private static final int LENGTH_ADJUSTMENT = 0;
    private static final int INITIAL_BYTES_TO_STRIP = 0;

    /**
     *
     */
    public CustomProtocolClient(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public void run() throws InterruptedException {

        EventLoopGroup workerGroup = new NioEventLoopGroup();

        try {
            Bootstrap b = new Bootstrap(); // (1)
            b.group(workerGroup); // (2)
            b.channel(NioSocketChannel.class); // (3)
            b.option(ChannelOption.TCP_NODELAY, true);
            b.option(ChannelOption.SO_KEEPALIVE, true);// (4)
            b.handler(new LoggingHandler(LogLevel.INFO));
            b.handler(new CustomProtocolClientInitializer());//初始化
            // 啟動 Client
            ChannelFuture f = b.connect(host, port).sync(); // (5)
            Channel channel = f.channel();

            // 传数据给服务端
            byte[] funcCode = "O".getBytes(StandardCharsets.UTF_8);
            byte[] brokerID = "800".getBytes(StandardCharsets.UTF_8);
            byte[] account = "1234567".getBytes(StandardCharsets.UTF_8);
            byte[] department = "ABED".getBytes(StandardCharsets.UTF_8);
            byte[] subAcc = "F".getBytes(StandardCharsets.UTF_8);
            byte[] exchange = "F".getBytes(StandardCharsets.UTF_8);
            byte[] bs1 = "F".getBytes(StandardCharsets.UTF_8);
            byte[] bs2 = "F".getBytes(StandardCharsets.UTF_8);
            byte[] commodity = "F".getBytes(StandardCharsets.UTF_8);
            byte[] comYM = "F".getBytes(StandardCharsets.UTF_8);
            byte[] comYM2 = "F".getBytes(StandardCharsets.UTF_8);
            byte[] strikePri = "F".getBytes(StandardCharsets.UTF_8);
            byte[] cp = "F".getBytes(StandardCharsets.UTF_8);
            byte[] qty1 = "F".getBytes(StandardCharsets.UTF_8);
            byte[] strategyPrice1 = "F".getBytes(StandardCharsets.UTF_8);
            byte[] qty2 = "F".getBytes(StandardCharsets.UTF_8);
            byte[] strategyPrice2 = "F".getBytes(StandardCharsets.UTF_8);
            byte[] priType1 = "F".getBytes(StandardCharsets.UTF_8);
            byte[] priType2 = "F".getBytes(StandardCharsets.UTF_8);
            byte[] timeInForce = "F".getBytes(StandardCharsets.UTF_8);
            byte[] openClose = "F".getBytes(StandardCharsets.UTF_8);
            byte[] dayTrade = "F".getBytes(StandardCharsets.UTF_8);
            byte[] orderDate = "F".getBytes(StandardCharsets.UTF_8);
            byte[] orderTime = "F".getBytes(StandardCharsets.UTF_8);
            byte[] seriesNo = "F".getBytes(StandardCharsets.UTF_8);
            byte[] triggerType1 = "F".getBytes(StandardCharsets.UTF_8);
            byte[] triggerType2 = "F".getBytes(StandardCharsets.UTF_8);
            byte[] triggerKind = "F".getBytes(StandardCharsets.UTF_8);
            byte[] triggerTicks = "F".getBytes(StandardCharsets.UTF_8);
            byte[] temp = "F".getBytes(StandardCharsets.UTF_8);
            byte[] userDef = "F".getBytes(StandardCharsets.UTF_8);
            short signatureLen = 2;
            byte[] signature = "0".getBytes(StandardCharsets.UTF_8);

            P529 p529 = new P529(funcCode, brokerID, account, department, subAcc, exchange,
                    bs1, bs2, commodity, comYM, comYM2, strikePri, cp,
                    qty1, strategyPrice1, qty2, strategyPrice2
                    , priType1, priType2, timeInForce, openClose, dayTrade, orderDate, orderTime,
                    seriesNo, triggerType1, triggerType2, triggerKind, triggerTicks,
                    temp, userDef, signatureLen, signature);

            CustomProtocolMessage msg = new CustomProtocolMessage(p529.toBytes(), p529.length(),p529.getType());

            log.info("[client] new p529 with Constructor: {}", p529);
            log.info("[client] new native msg: {}", msg);

            log.info("[client] byte[] p529 length:  {}, toBytes p529: {}", p529.length(), p529.toBytes());
            log.info("[client] byte[] msg  length:  {}, toBytes msg:  {}", msg.getLength(), msg.toBytes());

            //發送
            channel.writeAndFlush(msg);

            // 等待連線關閉
            channel.closeFuture().sync();
        } finally {
            workerGroup.shutdownGracefully();
        }
    }
}



