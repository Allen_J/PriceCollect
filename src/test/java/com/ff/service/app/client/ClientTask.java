package com.ff.service.app.client;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@NoArgsConstructor
@Slf4j
public class ClientTask implements Runnable {

    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        log.info("Client 啟動，開始工作");
        try {
            CustomProtocolClient client = new CustomProtocolClient("localhost", 8888);
            client.run();

        } catch (InterruptedException e) {

            e.printStackTrace();
        }
    }


}
